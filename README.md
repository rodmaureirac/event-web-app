# Event

Este es el repositorio oficial de la aplicación web "Event" desarrollada como proyecto para el ramo de ingeniería web impartido en la Pontifica Universidad Católica de Valparaíso. Event propone que los clientes puedan buscar y reclutar personal para organizar o atender su evento provado. De esta manera especialistas en el área de entretenimiento podrán prestar sus servicios para tu fiesta de graduación, matrimonio, babyshower, carrete de fin de semana, semana novata, etc.

### Requerimientos para montar el servidor

- Equipo con Microsoft Windows, Apple macOS o GNU/Linux 
- NodeJS 10 o superior
- Node Package Manager 6 o superior
- MongoDB 4 o superior

### Comando de instalación y ejecucción

        $ git clone https://bitbucket.org/rodmaureirac/event-web-app/
        $ cd ./event-web-app
        $ npm install
        $ node .

### Integrantes

- Rodrigo Maureira
- Felipe Céspedes
- Patricio Mardones
- Pedro Gallardo