/* 
 * Configuraciones
 * ---------------
 * 
 * Configuraciones varias del servidor y la base de datos
 * Inclye:  Puerto del servidor
 *          Dirección de la base de datos
 *          Clave súper secreta de encriptación para SHA1 (aún no implementada)
 *
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 *          Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 * 
 */

module.exports = {
    PORT: 3000,
    DATABASE: 'mongodb://thedisco.ddns.net/EventDataBase',
    HASH_KEY: 'heGOMthu7ceah'

    // TODO
};