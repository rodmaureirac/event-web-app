/*
 * Controlador de usuarios
 * -----------------------
 * 
 * Script referente al control de los métodos o funciones que pueden ejercer los
 * distintos acotres dentro de la aplicación.
 * 
 * Ejemplo: Usuario -> Iniciar Sesión
 *                  -> Registrarse
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 *
 */

 /* Dependencias de usuarios */
const User = require('../models/user');     // Modelo del usuario
const token = require('../services/token'); // Token de autenticación del usuario


/* 
 * Función Registrarse
 * Permite a un usuario registrarse dento de la aplicación
 * @param username: nombre de usuario
 * @param email: email del usuario
 * @param phone: teléfono del usuario
 * @param passwd: contraseña
 * 
 * @return status: 500 -> error; 201 -> correcto + token de acceso
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 *          Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 */
function signUp(req, res) {

    /* 
     * Se crea un nuevo usuario
     * En base sus características
     * establecidas en el modelo /models/user.js
     * 
     */
    const user = new User({
        username: req.query.username,
        email: req.query.email,
        phone: req.query.phone,
        passwd: req.query.passwd
    });

    /* 
     * Se guarda el usuario dentro de MongoDB
     * Y se genera un código de acceso temporal /services/token.js
     * 
     */
    user.save((err) => {
        if (err) return res.status(500).send({ message: 'Error:' + err });
        return res.status(201).send({ token: token.generateToken(user) }); 
    });

}

/*
 * Función Iniciar Session
 * Permite a un usuario iniciar una sessión válida
 * @param email: email de registro
 * @param passwd: contraseña de registro
 * 
 * @return status: 500 -> error, 200 -> ok + token de acceso temporal
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 *          Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 */
function signIn(req, res) {

    /* 
     * Busca al usuario
     * dentro de la base de datos
     * a corde a su modelo,
     * luego genera un token de
     * autenticación temporal
     */ 
    User.find({ email: req.query.email, passwd: req.query.passwd }, (err, user) => {
        if (err) return res.status(500).send({ message: err });
        if (user.length == 0) return res.status(404).send({ message: 'Error autenticacion' });
        res.user = user;
        res.status(200).send({
            message: 'Login exitoso',
            token: token.generateToken(user)
        });
    });
}

module.exports = {
    signIn,
    signUp
}