/* Middle Ware de Autenticación
 * ----------------------------
 * 
 * Módulo que permite verificar la autenticación
 * ante un petición del usuario.
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 * 
 */

/*
 * La autenticación está basada a modode de OAuth tal como los
 * Servicio de Google, Facebook, GitHub, entre otros. La autenticación
 * se base en tokens, cada token (/service/token.js) contiene 
 * información encriptada que permite validar la sesión del usuario
 *
 */
const token = require('../services/token');


/*
 * Función isAuth
 * Verifica si el usuario tiene un token de acceso válido para
 * solicitar información del servidor.
 * Los tokens deben viajar en el header de la petición GET
 *
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 * 
 */ 
function isAuth(req, res, next) {

    /* Obtiene el token
     * dentro del header
     * de la petición
     */ 
    if(!req.headers.auth) return res.status(403).send({ message: 'Sin permiso' });
    const t = req.headers.auth.split(" ")[1];
    
    /* Decodifica el token
     * obtenido y chequea
     * si es válido
     */ 
    token.decodeToken(t).then(response => {
        req.user = response;
        next();
    }).catch(res => {
        res.status(response.status)
    })
}

module.exports = {
    isAuth
};