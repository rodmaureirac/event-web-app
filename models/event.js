/* Modelo de Evento
 * -----------------
 *
 * Los modelos son representaciones de los objetos
 * JavaScript tanto para la aplicación como para la base
 * de datos MongoDB.
 * 
 * Autores: Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 *
 */   

/* Requerimientos */
const mongoose = require('mongoose');   // Manejo de MongoDB
const { Schema, model } = mongoose;     // Esquemas y modelos de los eventos en mongo

/*
 * Estructura de los eventos en Mongo
 * en base a los requerimientos
 * de la aplicación
 *
 */ 
const EventSchema = new Schema({
    author_id: { type: ObjectId },                  // ID del autor
    serviceName: { type: String, unique: true },    // Nombre
    description: { type: String, select: false },   // Descripción
    pictures: [{ type: Array }],
    lastUpdate : Date,
    address: String,                                // Dirección
    city: String,                                   // Ciudad
    locationX: String,
    locationY: String
});

module.exports = model('Event', EventSchema);