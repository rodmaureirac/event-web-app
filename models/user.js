/* Modelo de Usuario
 * -----------------
 *
 * Los modelos son representaciones de los objetos
 * JavaScript tanto para la aplicación como para la base
 * de datos MongoDB.
 * 
 * Autores: Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 *
 */   

/* Requerimientos */
const mongoose = require('mongoose');       // Maneja base de datos
const { Schema, model } = mongoose;         // Esquema y modelo de los usuarios en Mongo
const ObjectId = Schema.ObjectId;           // Identificador del usuario en Mongo
const bcrypt = require('bcrypt-nodejs');    // Para Encryptar ***Pendiente

/*
 * Estructura de un usuario dentro de
 * la base de datos de la aplicación
 * 
 */
const UserSchema = new Schema({
    evento_id: [{ type: ObjectId }],                            // ID Autogenerado
    username: { type: String, required: true, unique: true },   // Nombre de usuario
    passwd: { type: String, required: true, select: false },    // Contraseña
    email: { type: String, required: true, unique: true },      // Email
    phone: String,
    avatar: String,
    singupDate: { type: Date, default: Date.now()},
    name: String,
    lastLogin : Date
});

/*
 * --- EN DESARROLLO ---------
 *
 * Payload para guardar al usuario dentro de la base de datos,
 * busca encryptar su contraseña mediante SHA1 sólo en caso de
 * ser modificada.
 * 
 * --- EN DESARROLLO ---------
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 *          Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 */ 


UserSchema.pre('save', (next) => {
    let user = this;
    //if (!user.isModified('passwd')) return next();
    bcrypt.genSalt(10, (err, salt) => {
        if (err) return next();
        bcrypt.hash(user.passwd, salt, null, (err, hash) => {
            if (err) return next(err);
            user.passwd = hash;
            next();
        });
    })
});

module.exports = mongoose.model('user', UserSchema);