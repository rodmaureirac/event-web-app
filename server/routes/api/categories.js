const express = require('express');
const router = express.Router();

const _users = [
    {
        id: '1',
        name: 'Baby Shower',
    },
    {
        id: '2',
        name: 'Infantil',
    },
    {
        id: '3',
        name: 'Adolescente',
    },
    {
        id: '4',
        name: 'Universidad',
    },
    {
        id: '5',
        name: 'Graduación',
    },
    {
        id: '6',
        name: 'Masivo',
    },
    {
        id: '7',
        name: 'Matrimonio',
    }
];


router.get('/', (req, res) => {
    res.send({users: _users});
});


module.exports = router;