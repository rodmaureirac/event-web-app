const express = require('express');
const router = express.Router();

const _events = [
    {
        id: '1',
        title: 'Semana Mechona PUCV',
        author: '2',
        description: 'Se solicita animadores, DJ y bartenders para evento de bienvenida PUCV'
    },
    {
        id: '2',
        title: 'Matrimonio',
        author: '3',
        description: 'Se requiere personal para organizar matrimonio, coctel, música, productora'
    },
    {
        id: '3',
        title: 'Graduación Cuarto Medio',
        author: '1',
        description: 'Cocetl, dj, bartender, productora'
    }
];


router.get('/', (req, res) => {
    res.send({events: _events});
});


module.exports = router;