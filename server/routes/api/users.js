const express = require('express');
const router = express.Router();

const _users = [
    {
        id: '1',
        name: 'Rodrigo Maureira',
        email: 'rodmaureirac@gmail.com'
    },
    {
        id: '2',
        name: 'Felipe Céspedes',
        email: 'felipecespedes97@gmail.com'
    },
    {
        id: '3',
        name: 'Patricio Mardones',
        email: 'patricio.mardones.g@mail.pucv.cl'
    },
    {
        id: '4',
        name: 'Pedro Gallardo',
        email: 'pedrohirobuy@gmail.com'
    }
];


router.get('/', (req, res) => {
    res.send({users: _users});
});


module.exports = router;