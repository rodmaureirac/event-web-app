/*
 * WebService de Event + API
 * -------------------------
 * 
 * El WebService de Event es un servidor HTTP (localhost:3000) que se tiene como finalidad
 * servir y satisfacer las necesidades tanto de la API de Event como de su aplicación Web.
 * Puede encontrar todo lo relacionado en "/server" y "/public"
 *              
 * La API Event (localhost:3000/api) es un módulo que permite realizar consultas a la base
 * de datos (MongoDB) de manera segura (RestAPI). De esta manera la aplicación web puede consumir
 * los diversos servicios registrados en los servidores de Event y mostrarlos al público.
 * La API Event también está disponible al público, esto implica que los clientes podrán mostrar
 * información de sus perfiles en sus landing pages (como Twitter).
 * Puede contrar lo relacionado a la API Event en "/server/routes/api", "/middleware" y "/services".
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 *          Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 * 
 * Tecnología: ExpresJs, MongoDB
 * 
 */

/*
 * HTTP
 * Express es un framework de JavaScript que permite implementar
 * Servidores HTTP relativamente personalizables sin la necesidad de implementar
 * grandes lineas de código. A continuación sus dependencias
 * 
 */
const express = require('express');     // Módulo Express
const app = express();                  // Módulo para la aplicación web Event (ejecutable)
const cors = require('cors');           // Módulo para fijar "rutas" -> localhost:3000/esta/es/una/ruta
const mongoose = require('mongoose');   // Módulo para interacutas con Mongo (mongodb:/localhost/EventDataBase)
const parser = require('body-parser');  // Módulo para implementar comunicaciones mediante JSON

/*
 * API
 * La API consiste en mostrar información desde mongo hacia el cliente de manera segura,
 * esto es entregar información codificada en JSON en base a las peticiones del cliente
 * a continuación sus dependecias
 * 
 */
const users = require('./routes/api/users');            // Información sobre los usuarios
const events = require('./routes/api/events');          // Información sobre los eventos
const categories = require('./routes/api/categories');  // Información sobre las categorías

/*
 * Controladores y middleware
 * Los controladors se definen los "métodos" a implementar dentro de la aplicación. 
 * Por otro lado un middleware es un fragmento de código que se ejecuta
 * antes de ejecutar una determinada función (similar a un payload), esto es usado
 * para verificar los permisos del usuario sobre sus peticiones
 * 
 */ 
const auth = require('../middleware/auth');         // Autenticación (middleware)
const userCtrl = require('../controllers/user');    // Controlador del usuario 
const config = require('../config/config');         // Configuraciones generales -> /config

/*
 * Rutas
 * Para una RestAPI Las rutas son una forma rápida de interactuar con el servidor, para mayor comodidad
 * las "rutas" fueron dividas en código separado
 * 
 */
app.use(cors());                            // Permite implementar rutas en la aplicación
app.use('/api/users', users);               // Usuario
app.use('/api/events', events);             // Evento
app.use('/api/categories', categories);     // Categorías
app.use('/api/signup', userCtrl.signUp);    // Registrarse
app.use('/api/signin', userCtrl.signIn);    // Logearse
app.use(express.static('public'));          // Ruta "/", es decir el sitio web -> /public


// TESTING! para autenticación !!!!!!
app.use('/api/private', auth.isAuth, (req, res) => {
    res.status(200).send({ message: 'Permiso concedido' });
});


/* 
 *
 * Método de configuracion inicial
 * Primeramente, la aplicación intentará establecer conexión con la base de datos
 * posteriormente levantar el WebServer de Event en el puerto 3000,
 * si todo sale bien, podremos ingresar a http://localhost:3000/ para ver Event ejecutandose
 * 
 * @autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 *           Felipe Céspedes Cordero <felipecespedes97@gmail.com>
 * 
 * Las configuraciones están dentro de /config
 * 
 */
mongoose.set('useCreateIndex', true);
mongoose.connect(config.DATABASE, { useNewUrlParser: true }, (err) => {

    if (err) throw err;
    console.log('Connected to MongoDB');

    app.listen(config.PORT, (err) => {
        if(err) throw err;
        console.log(`Event WebServer Running at port ${config.PORT}`);
    });

});