/* Tokens de Autenticación
 * -----------------------
 *
 * Un token es un codigo cifrado que contiene información
 * sobre la sesión del usuario. Los tokens se generan automaticamente
 * a modo de payload durante el login o registro de un usurio en
 * el servidor
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 * 
 */

/* Requerimientos */
const jwt = require('jwt-simple');          // JSON Web Tokens
const moment = require('moment');           // Control del tiempo
const config = require('../config/config'); // Configuración general -> /config


/* 
 * Funcion genradora de tokens
 * Los tokens son generados en base al id del usario registrado
 * en la base de datos
 * 
 * @param user: El usuario que genera el token
 * @return jwt: El token
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 * 
 */ 
function generateToken(user) {

    /* Los tokens se registran
     * con la siguiente
     * información
     */
    const payload = {
        // ID Usuario
        sub: user._id,
        // Fecha creación
        iat: moment().unix,
        // Fecha de expiración
        exp: moment().add(7, 'days').unix()
    }
    return jwt.encode(payload, config.HASH_KEY);
}


/* Funcion para decodificar un token
 * Busca decodificar la información de un token para
 * verificar su validez.
 * 
 * @param token: el token
 * 
 * @return decoded: 500 -> no valido, 401 -> expirado, token decodificado
 * 
 * Autores: Rodrigo Maureira Contreras <rodmaureirac@gmail.com>
 * 
 */
function decodeToken(token) {

    /* 
     * Promesa decodificadora de
     * de tokens, busca corroborar validez
     *
     */
    const decoded = new Promise((res, rej) => {

        try 
        {
            /* Decodifica
             * Verifica fecha de expiración
             */
            const payload = jwt.decode(token, config.HASH_KEY);
            if(payload.exp < moment().unix()) { res({ status: 401, message: 'Token expirado' }); }
            res(payload.sub);
        } 
        catch (err) 
        {
            rej({ status: 500, message: 'Token invalido' });
        }

    });

    return decoded;
}

module.exports = {
    decodeToken,
    generateToken
}